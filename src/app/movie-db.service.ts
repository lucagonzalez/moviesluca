import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"

import { map } from "rxjs/operators"

@Injectable({
  providedIn: 'root'
})
export class MovieDBService {

  private apiKey: string = "067be2305fe7df53b3509fb4181121f1";
  private apiURL: string = "https://api.themoviedb.org/3";
  private callback: string = "callback=JSONP_CALLBACK"

  constructor(private http: HttpClient) { }

  getQuery(query: string) {
    
    const url = `${this.apiURL}${query}&api_key=${this.apiKey}&${this.callback}`;
    //const url = `https://api.themoviedb.org/3${query}&api_key=${this.apiKey}&callback=JSONP_CALLBACK`;

    return this.http.jsonp(url, "");
  }

  getQueryFilm(query: string) {
    const url = `${this.apiURL}${query}?api_key=${this.apiKey}&${this.callback}`;

    return this.http.jsonp(url, "");
  }

  getPopularMovies() {
    return this.getQuery("/discover/movie?sort_by=popularity.desc").pipe(map((data: any) => data.results));

  }

  getFilm(id: string) {
    return this.getQueryFilm(`/movie/${id}`).pipe(map((data: any) => data));
  }

  getMovieComments(id: string) {
    const url = `http://localhost:3000/movies/${id}?${this.callback}`;
    console.log("hi");
    return this.http.jsonp(url, "");
  }
  
}
