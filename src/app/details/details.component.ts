import { Component, OnInit } from '@angular/core';
import { MovieDBService } from '../movie-db.service';
import { ActivatedRoute } from '@angular/router';
import { CommentStmt } from '@angular/compiler';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  movie: any = {};
  comments: any = [];

  constructor(private route: ActivatedRoute, private movieService: MovieDBService) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      console.log(params);

      this.movieService.getFilm(params['id']).subscribe(movie => {
        console.log(movie);
        this.movie = movie;
      })
    })

    this.route.params.subscribe(params => {
      console.log("params", params);

      this.movieService.getMovieComments(params['id']).subscribe( comments => {
        console.log("comments", comments);
        this.comments = comments;
      })
    })
  }

}
