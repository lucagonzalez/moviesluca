import { Component, OnInit } from '@angular/core';
import { MovieDBService } from '../movie-db.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  movies: any [] = [];
  imageURL: string;

  constructor(private movieService: MovieDBService, private router: Router) { }

  ngOnInit() {

    let imageURL = "http://image.tmdb.org/t/p/w400";
    
    this.movieService.getPopularMovies().subscribe((data: any) => {

      console.log(data);
      this.movies = data;
    })
    
    }

  onSelect(movie: any){

    let movieId = movie.id;

    this.router.navigate(['/details', movieId]);
  }

}
